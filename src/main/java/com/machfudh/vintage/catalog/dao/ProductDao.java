/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.vintage.catalog.dao;

import com.machfudh.vintage.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Machfudh
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String>{
    
}
