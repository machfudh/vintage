/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Machfudh
 * Created: Aug 30, 2018
 */

create table product(
id VARCHAR(36),
code VARCHAR (100) NOT NULL,
name VARCHAR (255) NOT NULL,
PRIMARY KEY (id),
UNIQUE (code)
);
